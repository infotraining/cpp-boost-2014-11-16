#include <iostream>
#include <vector>
#include <list>
#include <iterator>
#include <functional>
#include <algorithm>
#include <boost/any.hpp>
#include <boost/range/adaptor/indexed.hpp>
#include <boost/range/algorithm.hpp>
#include <boost/range/adaptors.hpp>

using namespace std;

template <typename Type>
struct IsType
{
    typedef bool result_type;

    bool operator()(const boost::any& item) const
    {
        return item.type() == typeid(Type);
    }
};

int main()
{
	vector<boost::any> store_anything;

	store_anything.push_back(1);
	store_anything.push_back(5);
	store_anything.push_back(string("three"));
	store_anything.push_back(3);
	store_anything.push_back(string("four"));
	store_anything.push_back(string("one"));
	store_anything.push_back(string("eight"));
	store_anything.push_back(5);
	store_anything.push_back(4);
	store_anything.push_back(boost::any());
	store_anything.push_back(string("five"));
	store_anything.push_back(string("six"));
	store_anything.push_back(boost::any());


	/* TO DO :
     * Wykorzystując algorytmy biblioteki standardowej wykonaj nastapujące czynnosci (napisz odpowiednie
	 * do tego celu predykaty lub obiekty funkcyjne):
     * 1 - przefiltruj wartosci niepuste w kolekcji stored_anything
	 * 2 - zlicz ilosc elementow typu int oraz typu string
	 * 3 - wyekstraktuj z kontenera store_anything do innego kontenera wszystkie elementy typu string
	 */

	// 1
	vector<boost::any> non_empty;
	// TODO
	cout << "store_anything.size() = " << store_anything.size() << endl;

//    for(const auto& item : store_anything)
//    {
//        if (!item.empty())
//            non_empty.push_back(item);
//    }

    boost::remove_copy_if(store_anything, back_inserter(non_empty), mem_fn(&boost::any::empty));

    cout << "non_empty.size() = " << non_empty.size() << endl;

	// 2
    //int count_int = count_if(non_empty.begin(), non_empty.end(), IsType<int>());
    int count_int = boost::count_if(non_empty, IsType<int>());
	// TODO
    cout << "stored_anything przechowuje " << count_int << " elementow typu int" << endl;

    int count_string = count_if(non_empty.begin(), non_empty.end(), IsType<string>());
	// TODO
    cout << "stored_anything przechowuje " << count_string << " elementow typu string" << endl;

	// 3
    list<string> string_items;

//    for(const auto& item : non_empty)
//    {
//        if (const string* string_item = boost::any_cast<string>(&item))
//        {
//            string_items.push_back(*string_item);
//        }
//    }

    string (*convert_to_string)(const boost::any&) = &boost::any_cast<string>;

    boost::transform(non_empty
                        | boost::adaptors::filtered(IsType<string>())
                        | boost::adaptors::reversed,
                     back_inserter(string_items),
                     convert_to_string);


	cout << "string_items: ";

    using namespace boost::adaptors;

//    for(const auto& item : string_items | indexed(0))
//    {
//        cout << "Index: " << item.index() << " - " << item.value() << endl;
//    }

    for(const auto& item : string_items)
    {
        cout << "Item: " << item << endl;
    }

    cout << endl;
}
