#include <iostream>
#include <string>
#include <vector>
#include <boost/any.hpp>

using namespace std;

int main()
{
    boost::any object = 4;

    if (object.empty())
        cout << "object is empty..." << endl;
    else
        cout << "object has value..." << endl;


    object = 3.14;

    short sx = 18;

    object = sx;

    string str = "Hello";

    object = str;

    vector<string> vec = { "one", "two", "three" };

    object = vec;

    object = 6;


    // 1st version of any_cast
    try
    {
        int int_value = boost::any_cast<int>(object);
        cout << "int_value: " << int_value << endl;
    }
    catch(const boost::bad_any_cast& e)
    {
        cout << "Exception: " << e.what() << endl;
    }

    int* ptr_int = boost::any_cast<int>(&object);

    if (ptr_int)
        cout << "int_value: " << *ptr_int << endl;

    ptr_int = boost::unsafe_any_cast<int>(&object);

    cout << "type in object: " << object.type().name() << endl;

    object.clear();
}

