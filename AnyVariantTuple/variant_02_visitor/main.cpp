#include <iostream>
#include <vector>
#include <complex>
#include <algorithm>
#include <boost/variant.hpp>
#include <boost/any.hpp>

using namespace std;

class AnySerializer
{
public:
    void operator()(const boost::any& item) const
    {
        if (const int* int_item = boost::any_cast<int>(&item))
        {
            cout << "int: " << *int_item << endl;
        }
        else if (const string* string_item = boost::any_cast<string>(&item))
        {
            cout << "string: " << *string_item << endl;
        }
    }
};


class VariantSerializer : public boost::static_visitor<>
{
public:
    void operator()(const int& item) const
    {
        cout << "int: " << item << endl;
    }

    void operator()(const string& item) const
    {
        cout << "string: " << item << endl;
    }

    void operator()(const complex<double>& item) const
    {
        cout << "complex<double>: " << item.real() << " + i" << item.imag() << endl;
    }
};

int main()
{
   using namespace boost;

   vector<any> any_items = { 1, 2, string("One"), 4, string("Two"), 5.44 };

   for_each(any_items.begin(), any_items.end(), AnySerializer());

   cout << "\nVisiting variant:\n";

   using MyVariant =  variant<int, string, complex<double>>;

   MyVariant var1 = complex<double>{1,4};

   apply_visitor(VariantSerializer(), var1);

   vector<MyVariant> variant_items = { 1, 5, string("Text"), complex<double>{8, 9}, var1 };

   VariantSerializer serializer;
   for_each(variant_items.begin(), variant_items.end(), apply_visitor(serializer));
}
