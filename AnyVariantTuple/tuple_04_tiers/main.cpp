#include <iostream>
#include <string>
#include <boost/tuple/tuple.hpp>
#include <boost/algorithm/string.hpp>

using namespace std;

boost::tuple<int, string> foo()
{
	return boost::make_tuple(10, "Zenon Kowalski");
}

int main()
{
	int id;
	string name;

	boost::tie(id, name) = foo();

	cout << "id = " << id << "; name = " << name << endl;

	// tie dziala rowniez z pair
	boost::tie(id, name) = make_pair(1, "Nikodem Dyzma");

	cout << "id = " << id << "; name = " << name << endl;

	// tuple::ignore

	string person;

	boost::tie(boost::tuples::ignore, person) = foo();

	cout << "name = " << person << endl;
}

