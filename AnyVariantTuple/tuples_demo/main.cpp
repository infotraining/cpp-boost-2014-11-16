#include <iostream>
#include <algorithm>
#include <numeric>
#include <boost/tuple/tuple.hpp>
#include <boost/tuple/tuple_comparison.hpp>
#include <boost/tuple/tuple_io.hpp>

using namespace std;

boost::tuple<int, int, double> calc_stats(const vector<int>& data)
{
    vector<int>::const_iterator min_it;
    vector<int>::const_iterator max_it;

    boost::tie(min_it, max_it) = minmax_element(data.begin(), data.end());
    auto avg = accumulate(data.begin(), data.end(), 0.0) / data.size();

    return boost::make_tuple(*min_it, *max_it, avg);
}

class Person
{
    int id_;
    string first_name_;
    string last_name_;
public:
    Person(int id, const string& fn, const string& ln)
        : id_{id}, first_name_{fn}, last_name_ {ln}
    {}

    void print() const
    {
        cout << "Person(id: " << id_ << ", " << first_name_ << " " << last_name_ << ")" << endl;
    }

    bool operator<(const Person& p) const
    {
//        if (id_ == p.id_)
//        {
//            if (last_name_ == p.last_name_)
//            {
//                return first_name_ < p.first_name_;
//            }
//            else
//                return last_name_ < p.last_name_;
//        }

//        return id_ < p.id_;

        return tied() < p.tied();
    }

    bool operator==(const Person& p) const
    {
        return tied() == p.tied();
    }
private:
    boost::tuple<const int&, const string&, const string&> tied() const
    {
        return boost::tie(id_, last_name_, first_name_);
    }
};


int main()
{
    vector<int> data = { 4, 5, 1, 2, 8, 9, 22, 34, 0, 21, 665, 8 };

    boost::tuple<int, int, double> stats = calc_stats(data);

    cout << boost::tuples::set_open('{') << boost::tuples::set_close('}')
         << "stats: " << stats << endl;

    cout << "min: " << stats.get<0>() << endl;
    cout << "max: " << stats.get<1>() << endl;
    cout << "avg: " << boost::tuples::get<2>(stats) << endl;

    cout << "\n\n";

    data.insert(data.end(), {-8, -88, 23423, 224, -767});

    int min_value;
    int max_value;
    double avg_value;

    //boost::tuple<int&, int&, double&> ref_tuple(min_value, max_value, avg_value);
    //ref_tuple = calc_stats(data);
    boost::tie(min_value, max_value, avg_value) = calc_stats(data);

    cout << "min: " << min_value << endl;
    cout << "max: " << max_value << endl;
    cout << "avg: " << avg_value << endl;

    cout << "\n\n";

    data.insert(data.end(), { 888, 2342, 5345, -34545 });

    boost::tie(min_value, max_value, boost::tuples::ignore) = calc_stats(data);

    cout << "min: " << min_value << endl;
    cout << "max: " << max_value << endl;

    vector<Person> people = { Person{1, "Jan", "Kowalski"}, Person{4, "Adam", "Nowak"},
                              Person{4, "Ewa", "Anonim"}, Person{1, "Anna", "Zet"} };

    sort(people.begin(), people.end());

    for(const auto& p : people)
    {
        p.print();
    }
}

