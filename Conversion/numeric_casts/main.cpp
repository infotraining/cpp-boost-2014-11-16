#include <iostream>
#include <boost/cast.hpp>

using namespace std;

int main() try
{
    int x = 7322;
    short sx = boost::numeric_cast<short>(x);

    cout << "x: " << x << endl;
    cout << "sx: " << sx << endl;

    x = -2;

    unsigned int ux = boost::numeric_cast<unsigned int>(x);

    cout << "x: " << x << endl;
    cout << "ux: " << ux << endl;
}
catch(boost::bad_numeric_cast& e)
{
    cout << "Exception: " << e.what() << endl;
}
