#include <boost/shared_ptr.hpp>
#include <boost/make_shared.hpp>
#include <vector>
#include <iostream>
#include <cstdlib>
#include <algorithm>

using namespace std;

class Object
{
public:
    Object(int val = 0, const string& name = "unkown")
        : val_(val), name_(name)
	{
		cout << "Konstrukcja obiektu Object " << val_ << "\n";
	}
	
	~Object()
	{
		cout << "Destrukcja obiektu Object " << val_ << "\n";
	}
	
	void print() const
	{
		cout << "Działa obiekt klasy Object " << val_ << "\n";
	}
private:
	int val_;
    string name_;
};

int main()
{
	boost::shared_ptr<Object> external;
	try
	{
        std::vector< boost::shared_ptr<Object> > vec =
            { boost::make_shared<Object>(1, "obj1"),
              boost::make_shared<Object>(2, "obj2"),
              boost::make_shared<Object>(3, "obj3") };

        vec.emplace_back(new Object(4, "obj4"));
		
		vec[0]->print();

		external = vec[0];

        vector<Object> objects;

        objects.push_back(Object{4, "obj4"});
        objects.emplace_back(4, "obj4");
		
		throw int(5);
	}
	catch(...)
	{
		cout << "Zlapalem wyjątek\n";
	}
}
