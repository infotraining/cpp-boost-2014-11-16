#include <iostream>
#include <set>
#include <boost/lexical_cast.hpp>
#include <memory>
#include <cassert>
#include <cstdlib>
#include <stdexcept>

class Observer
{
public:
    virtual void update(const std::string& event_args) = 0;
    virtual ~Observer() {}
};

class Subject
{
    int state_;
    //std::set<boost::weak_ptr<Observer>> observers_;
    std::set<std::weak_ptr<Observer>, std::owner_less<std::weak_ptr<Observer>>> observers_;

public:
    Subject() : state_(0)
    {}

    void register_observer(std::weak_ptr<Observer> observer)
    {
        observers_.insert(observer);
    }

    void unregister_observer(std::weak_ptr<Observer> observer)
    {
        observers_.erase(observer);
    }

    void set_state(int new_state)
    {
        if (state_ != new_state)
        {
            state_ = new_state;
            notify("Changed state on: " + boost::lexical_cast<std::string>(state_));
        }
    }

protected:
    void notify(const std::string& event_args)
    {
        auto it = observers_.begin();

        while (it != observers_.end())
        {
            std::shared_ptr<Observer> observer = it->lock();

            if (observer)
            {
                observer->update(event_args);
                ++it;
            }
            else
            {
                //it = observers_.erase(it); // C++11
                observers_.erase(it++); // C++03
            }
        }


        //      for(set<shared_ptr<Observer>>::iterator it = observers_.begin();
        //          it != observers_.end(); ++it)
        //      {
        //          const auto& observer = *it;
        //          (*it)->update(event_args);
        //      }
    }
};

class ConcreteObserver1
        : public Observer, public std::enable_shared_from_this<ConcreteObserver1>
{
public:
    virtual void update(const std::string& event)
    {
        std::cout << "ConcreteObserver1: " << event << std::endl;
    }

    void register_me(Subject& s)
    {
        s.register_observer(shared_from_this());
    }
};

class ConcreteObserver2 : public Observer
{
public:
    virtual void update(const std::string& event)
    {
        std::cout << "ConcreteObserver2: " << event << std::endl;
    }
};

void use(std::shared_ptr<Observer> o)
{
    o->update("Using object");
}

std::shared_ptr<Observer> external_observer = std::make_shared<ConcreteObserver1>();

void reset()
{
    external_observer = std::make_shared<ConcreteObserver2>();
}

void use2(Observer& o);

void use1(Observer& o)
{
    std::cout << "use1: " << &o << std::endl;
    o.update("Using object");

    reset();

    use2(o);
}

void use2(Observer& o)
{
    std::cout << "use2: " << &o << std::endl;
    o.update("Using object");
}

void foo()
{
    std::shared_ptr<Observer> pinned_observer = external_observer;
    use1(*pinned_observer);
}

std::unique_ptr<Observer> observer_factory(const std::string& id)
{
    if (id == "O1")
        return std::unique_ptr<Observer>{new ConcreteObserver1 };
    else if (id == "O2")
        return std::unique_ptr<Observer>{new ConcreteObserver2 };

    throw std::runtime_error{"Bad observer id"};
}

int main(int argc, char const *argv[])
{
    foo();

    using namespace std;

    Subject s;

    ConcreteObserver1 co1;

    auto o1 = make_shared<ConcreteObserver1>();
    o1->register_me(s);

    {
        auto o2 = shared_ptr<Observer>{observer_factory("O2")};
        s.register_observer(o2);

        s.set_state(1);

        cout << "End of scope." << endl;
    }

    s.set_state(2);
}
