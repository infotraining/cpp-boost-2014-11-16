#include <iostream>
#include <cstdlib>
#include <exception>
#include <stdexcept>
#include <memory>
#include <boost/noncopyable.hpp>

using namespace std;

/********************************************************************
*  Uodparnianie konstrukora na wyjątki #1
********************************************************************/

class Device
{
private:
	size_t devno_;
public:
	Device(int devno) : devno_(devno)
	{
		if (devno == 2)
			throw std::runtime_error("Powazny problem!");

		cout << "Konstruktor Device #" << devno << endl;
	}

	~Device()
	{
		cout << "Destruktor Device #" << devno_ << endl;
	}

};

class Broker : boost::noncopyable
{
public:
    Broker(int devno1, int devno2)
        : dev1_(new Device(devno1)), dev2_(new Device(devno2))
	{
	}

//    Broker(const Broker&) = delete;
//    Broker& operator=(const Broker&) = delete;

//    Broker(Broker&& source) : dev1_{move(source.dev1_)}, dev2_{move(source.dev2_)}
//    {}

//    Broker& operator=(Broker&& source)
//    {
//        dev1_ = move(source.dev1_);
//        dev2_ = move(source.dev2_);
//    }

//    ~Broker()
//    {
//        cout << "~Broker()" << endl;
//    }

    void info() const
    {
        cout <<  "dev1: " << dev1_.get() << endl;
        cout <<  "dev2: " << dev2_.get() << endl;
    }
private:
    std::unique_ptr<Device> dev1_;
    std::unique_ptr<Device> dev2_;
};

int main()
{
	try
	{
        Broker b(1, 3);
        b.info();

        Broker b2 = move(b);
        cout << "b po move(b):\n";
        b.info();

        cout << "b2 po move(b):\n";
        b2.info();
	}
	catch(const exception& e)
	{
		cerr << "Wyjatek: " << e.what() << endl;
	}
}
