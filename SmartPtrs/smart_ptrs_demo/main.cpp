#include <iostream>
#include <memory>
#include <stdexcept>
#include <cassert>
#include <vector>
#include <boost/scoped_ptr.hpp>
#include <boost/scoped_array.hpp>

using namespace std;

class Widget
{
    int id_;
public:
    Widget(int id = 0) : id_(id)
    {
        cout << "Widget(id: " << id_ << ")" << endl;
    }

    virtual ~Widget()
    {
        cout << "~Widget(id: " << id_ << ")" << endl;
    }

    int id() const
    {
        return id_;
    }

    virtual void run()
    {
        std::cout << "Widget::run()" << std::endl;
    }
};

class SuperWidget : public Widget
{
public:
    using Widget::Widget;

    ~SuperWidget()
    {
        std::cout << "~SuperWidget(id: " << Widget::id() << ")" << std::endl;
    }

    void super_method()
    {
        cout << "SuperWidget::super_method()" << endl;
    }

    virtual void run() override
    {
        std::cout << "SuperWidget::run()" << std::endl;
    }
};

void client(Widget& w)
{
    cout << "Client is using widget: " << w.id() << endl;
}


void may_throw(int arg)
{
    if (arg == 13)
        throw runtime_error("Error#13");
}

std::auto_ptr<Widget> widget_factory(int id)
{
    cout << "Creating widget...\n";
    return std::auto_ptr<Widget>(new Widget(id));
}

void use_widget(std::auto_ptr<Widget> w)
{
    cout << "Using widget: " << w->id() << " with auto_ptr...";
}

void auto_ptr_demo()
{
    auto_ptr<Widget> aptr1 = widget_factory(1);

    cout << "Id: " << (*aptr1).id() << endl;

    use_widget(widget_factory(3));

    client(*aptr1);

    aptr1.reset(new Widget(2));

    client(*aptr1);

    auto_ptr<Widget> aptr2 = aptr1;

    client(*aptr2);

    assert(aptr1.get() == 0);

    Widget* ptr = aptr2.release();

    delete ptr;

    cout << "End of scope..." << endl;
}

std::unique_ptr<Widget> unique_widget_factory(int id)
{
    return std::unique_ptr<Widget>{new Widget{id}};
}

void use_unique_widget(std::unique_ptr<Widget> w)
{
    cout << "Using widget: " << w->id() << " with unique_ptr...";
}

void unique_ptr_demo()
{
    unique_ptr<Widget> uptr1 {new Widget{1}};
    cout << "Id: " << uptr1->id() << endl;

    unique_ptr<Widget> uptr2 = move(uptr1);

    client(*uptr2);
    assert(uptr1.get() == nullptr);

    unique_ptr<Widget> uptr3 = unique_widget_factory(3);
    client(*uptr3);

    use_unique_widget(move(uptr3));

    assert(uptr3.get() == nullptr);

    use_unique_widget(unique_widget_factory(9));

    unique_ptr<Widget> uptr4 = widget_factory(665);

    cout << "\nCreating vector of unique_ptrs..." << endl;

    vector<unique_ptr<Widget>> widgets;

    widgets.push_back(move(uptr4));  // push_back(const T&)
    widgets.push_back(unique_ptr<Widget>{new Widget{999}}); // push_back(T&&)
    widgets.push_back(unique_widget_factory(555));
    widgets.emplace_back(new Widget{333});

    for(const auto& w : widgets)
    {
        client(*w);
    }

    unique_ptr<Widget[]> arr1 { new Widget[10] };
    cout <<  "Id: " << arr1[0].id() << endl;
}

void scoped_ptr_demo()
{
    boost::scoped_ptr<Widget> scptr1(new Widget{777});

    //boost::scoped_ptr<Widget> scptr2 = move(scptr1);

    scptr1.reset(new Widget{999});

    client(*scptr1);

    boost::scoped_array<Widget> arr1 { new Widget[10] };

    cout << "Id: " << arr1[0].id() << endl;
}

int main()
{
    cout << "\n\n";

    //unique_ptr_demo();

    {
        {
            std::shared_ptr<Widget> w1 = std::make_shared<Widget>();
            w1->run();

            std::shared_ptr<SuperWidget> sw1 = dynamic_pointer_cast<SuperWidget>(w1);

            if (sw1)
                sw1->super_method();
        }
    }

    cout << "End of main..." << endl;
}

