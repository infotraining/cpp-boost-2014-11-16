#include <iostream>
#include <vector>
#include <boost/bind.hpp>
#include <boost/assign.hpp>

using namespace std;
using namespace boost::assign;

bool check_range(int i)
{
	return i > 0 && i < 10;
}

int main()
{
	cout.setf(ios::boolalpha);
	cout << "check_range(8) = " << check_range(15) << endl;
	cout << "check_range(15) = " << check_range(15) << endl;

	vector<int> numbers;
	numbers += 1, 5, 7, 33, 23, 7, 9, 19, 77, 88, 2;

	remove_copy_if(numbers.begin(),
				   numbers.end(),
				   ostream_iterator<int>(cout, " "),
				   &check_range);
	cout << "\n";

    // to samo z wykorzystaniem kompozycji wykorzystujacej bind
	remove_copy_if(numbers.begin(),
					   numbers.end(),
					   ostream_iterator<int>(cout, " "),
					   boost::bind(
							   logical_and<bool>(),
							   boost::bind(greater<int>(), _1, 0),
							   boost::bind(less<int>(), _1, 10)
					   )
	);
}


