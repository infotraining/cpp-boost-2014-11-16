#include <iostream>
#include <vector>
#include <algorithm>
#include <functional>
#include <boost/bind.hpp>

using namespace std;

void func2(const std::string& prefix, int value)
{
    cout << "func2: " << prefix << " - " << value << endl;
}

double calculate(int x, double y, int& counter)
{
    ++counter;

    cout << "calculate(x: " << x << ", y: " << y << ", counter: " << counter
         << ")" << endl;

    return x * y;
}

class Add
{
public:
    typedef int result_type;

    int operator()(int a, int b) const
    {
        return a + b;
    }
};

class Person
{
    int id_;
    string first_name_;
    string last_name_;
    int age_;
public:
    Person(int id, const string& fn, const string& ln, int age)
        : id_{id}, first_name_{fn}, last_name_ {ln}, age_{age}
    {}

    string last_name() const
    {
        return last_name_;
    }

    int age() const
    {
        return age_;
    }

    void print() const
    {
        cout << "Person(id: " << id_ << ", " << first_name_ << " " << last_name_ << ")" << endl;
    }

    void print_with_prefix(const string& prefix)
    {
        cout << prefix << " - id: " << id_ << ", " << first_name_ << " " << last_name_ << endl;
    }
};

//void print_person(const Person& p)
//{
//    p.print();
//}

int main()
{
    auto f1 = boost::bind(&func2, "Test bind", 665);

    f1();

    int x = 234;

    auto f2 = boost::bind(&func2, _1, x);

    f2("Argument opozniony");

    auto f3 = boost::bind(&func2, _1, _2);

    f3("Test3", 999);

    auto f4 = boost::bind(&func2, _2, _1);

    f4(234, "Test4");

    int counter = 0;

    auto f5 = boost::bind(&calculate, x, _1, boost::ref(counter));

    auto result = f5(3.14);

    cout << "result: " << result << "  - counter: " << counter << endl;

    auto f6 = boost::bind(Add(), 9, _1);

    cout << f6(10) << endl;

    cout << "\n\n";

    vector<Person> people = { Person{1, "Jan", "Kowalski", 44}, Person{4, "Adam", "Nowak", 33},
                              Person{4, "Ewa", "Anonim", 75}, Person{1, "Anna", "Zet", 12} };

    for_each(people.begin(), people.end(), boost::bind(&Person::print, _1));

    cout << "\n\n";

    for_each(people.begin(), people.end(),
             boost::bind(&Person::print_with_prefix, _1, "Osoba"));

    cout << "\n\n";

    vector<Person*> people_ptrs = { &people[0], &people[1], &people[2], &people[3] };

    for_each(people_ptrs.begin(), people_ptrs.end(),
             boost::bind(&Person::print_with_prefix, _1, "PtrOsoba"));


    vector<string> last_names;

    transform(people_ptrs.begin(), people_ptrs.end(), back_inserter(last_names),
              boost::bind(&Person::last_name, _1));

    cout << "\n\n";

    for(const auto& ln : last_names)
        cout << ln << endl;

    auto std_first_retired = find_if(people.begin(), people.end(),
                                 std::bind(greater<int>(),
                                                std::bind(&Person::age, std::placeholders::_1),
                                                67));

    auto first_retired = find_if(people.begin(), people.end(),
                                 bind(&Person::age, _1) > 67);

    if (first_retired != people.end())
    {
        cout << "first_retired: ";
        first_retired->print();
    }
}

