#include <iostream>
#include <boost/static_assert.hpp>
#include <boost/type_traits/is_base_of.hpp>
#include <boost/type_traits/is_integral.hpp>
#include <boost/type_traits/is_arithmetic.hpp>
#include <memory>
#include <string>

class Base {};

class Derived : public Base {};

class Different {};

template <typename T>
class OnlyCompatibleWithIntegralTypes
{
    //BOOST_STATIC_ASSERT(boost::is_integral<T>::value);

    static_assert(boost::is_integral<T>::value, "T is not integral type");
};

template <typename T, size_t i>
void accepts_arrays_with_size_between_1_and_100(T (&arr)[i])
{
	BOOST_STATIC_ASSERT(i>=1 && i<=100);
}

void expects_ints_to_be_4_bytes()
{
    BOOST_STATIC_ASSERT(sizeof(int) == 4);
}

template <typename T>
void works_with_base_and_derived(T& t)
{
	BOOST_STATIC_ASSERT((boost::is_base_of<Base, T>::value));
}

template <typename T>
struct is_auto_ptr : public boost::false_type
{};

template <typename T>
struct is_auto_ptr<std::auto_ptr<T>> : public boost::true_type
{};

template <typename Pointer>
void use(Pointer ptr)
{
    BOOST_STATIC_ASSERT(!is_auto_ptr<Pointer>::value);

    std::cout << "Using: " << *ptr << std::endl;
}

int main()
{
    OnlyCompatibleWithIntegralTypes<short> test1;

    int arr[50];

	accepts_arrays_with_size_between_1_and_100(arr);

	Derived arg;
    std::string str = "Hello";
    works_with_base_and_derived(arg);

    int x = 10;
    use(&x);

    std::shared_ptr<std::string> sp = std::make_shared<std::string>("SharedPtr");;
    use(sp);

//    std::auto_ptr<std::string> ap(new std::string("AutoPtr"));
//    use(ap);

//    std::cout << *ap << std::endl;
}
