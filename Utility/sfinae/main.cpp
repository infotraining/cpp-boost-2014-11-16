#include <iostream>
#include <typeinfo>
#include <boost/utility/enable_if.hpp>
#include <boost/type_traits/is_integral.hpp>

using namespace std;

struct Int
{
    typedef void nested_type;

    int value;
};

ostream& operator<<(ostream& out, const Int& i)
{
    out << i.value;

    return out;
}

template <typename T>
typename boost::enable_if<boost::is_integral<T>, T>::type foo(T value)
{
    cout << "foo(integral: " << value << ")" << endl;

    return value;
}

template <typename T>
typename boost::disable_if<boost::is_integral<T>, T>::type foo(T value)
{
    cout << "foo(not integral: " << typeid(T).name() << ": " << value << ")" << endl;

    return value;
}

int main()
{
    foo(7);

    int x = 10;
    foo(x);

    short sx = 99;
    foo(sx);

    double dx = 13.33;
    foo(dx);

    float fx = 43.6F;
    foo(fx);

//    Int i { 10 };
//    foo(i);
}

