#include <iostream>
#include <functional>

using namespace std;

void print0()
{
    cout << "print0()" << endl;
}

class Printer
{
public:
    void operator()()
    {
        cout << "Printer::operator()()" << endl;
    }
};

void print1(const string& p)
{
    cout << "print(" << p << ")" << endl;
}

int main()
{
    function<void()> f0;

    if (f0)
        f0();
    else
        cout << "f0 jest puste..." << endl;

    f0 = &print0;

    f0();

    f0 = Printer();

    f0();

    f0 = std::bind(&print1, "bound item");

    f0();

    f0 = [] { cout << "Lambda" << endl; };

    f0();

    auto bound_fun = std::bind(&print1, "bound item");
    bound_fun();
}

